require 'yaml'  

app_yml_path = "#{Dir.pwd}/config/application.yml" 
app_sec_path = "#{Dir.pwd}/config/secrets.yml"  

if File.exist? app_yml_path   
	CONFIG = YAML.load_file app_yml_path   
	case ENV['RACK_ENV']   
	when 'production'     
		ENV.update CONFIG['prod']     
		puts 'loaded configuration file for production'
	when 'development'
		puts CONFIG
		ENV.update CONFIG['dev']
		puts 'loaded configuration file for development'
	else
		puts "could not find config for env #{ENV['RACK_ENV']}"
	end
else
	puts 'could not find configuration file. skipping.'
end

if File.exist? app_sec_path
	SECRETS = YAML.load_file app_sec_path
	ENV.update SECRETS
	puts 'loaded secrets file'
else
	puts 'could not find secrets file. skipping.'
end


require './app'
run App.freeze.app

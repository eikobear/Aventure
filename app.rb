require 'pry'

require 'roda'
require 'json'

require './db/db'

Rack::Mime::MIME_TYPES.merge!({ '.mjs' => 'application/javascript; charset=utf-8' })

class App < Roda
  plugin :render
  plugin :public
  plugin :json_parser
  plugin :sessions, secret: ENV['RODA_SESSION_SECRET'] 

  route do |r|
    r.root do
      view :index, layout: :layout, locals: session[:user]
    end

    r.get 'whoami' do
      (session['user'] || {}).to_json
    end

    r.post 'signup' do
      data = DB.register_user(r.params['username'], 
                       r.params['email'], 
                       r.params['password'], 
                       r.params['confirmation'])

      session['user'] = data unless data[:errors]

      data.to_json
    end

    r.post 'login' do
      data = DB.authenticate(r.params['username'], r.params['password'])
      
      session['user'] = data unless data[:errors]

      data.to_json
    end

    r.get 'logout' do
      session.delete('user');
      {}.to_json
    end

    r.get 'projects' do
      DB.projects(session['user']).to_json
    end

    r.post "bits" do
      DB.bits(session['user'], r.params['project_id']).to_json
    end

    r.post "bit" do
      DB.bit(session['user'], r.params['bit_id']).to_json
    end

    r.post 'rename_bit' do
      DB.rename_bit(session['user'], r.params['bit_id'], r.params['name']).to_json
    end

    r.post 'save_bit' do
      DB.save_bit(session['user'], r.params['bit_id'], r.params['data']).to_json
    end

    r.post 'new_project' do
      DB.new_project(session['user'], r.params['name']).to_json
    end

    r.post 'delete_project' do
      DB.delete_project(session['user'], r.params['project_id']).to_json
    end

    r.post 'delete_bit' do
      DB.delete_bit(session['user'], r.params['bit_id']).to_json
    end

    r.post 'rename_project' do
      DB.rename_project(session['user'], r.params['project_id'], r.params['name']).to_json
    end

    r.post 'new_bit' do
      DB.new_bit(session['user'], 
                 r.params['project_id'], 
                 r.params['name'], 
                 r.params['index']).to_json
    end

    r.public

  end
end



console.log('bonne aventure!');

import map from '../lib/writeous/map.mjs';
import Writeous from '../lib/writeous/writeous.mjs';


if(document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', init);
}
else {
  init();
}

function init() {
  const writeous = new Writeous('writeous', map);
  writeous.updateScrollbar();
  window.writeous = writeous;

  window.modal = new Vue({
    el: '#modal',
    data: {
      hidden: true,
      message: 'Are you sure?',
      confirmText: 'yes',
      denyText: 'no',
      onConfirm: () => {},
      onDeny: () => {},
      activeElement: null,
    },
    methods: {
      display: function display(options = {}) {
        this.message = options.message || this.message;
        this.confirmText = options.confirmText || this.confirmText;
        this.denyText = options.denyText || this.denyText;
        this.onConfirm = options.confirm || this.onConfirm;
        this.onDeny = options.deny || this.onDeny;
        this.hidden = false;
        this.activeElement = document.activeElement;
        setTimeout(() => (document.getElementById('modaldenybutton').focus()), 0);
      },
      hide: function hide() {
        this.hidden = true;
      },
      confirm: function confirm() {
        this.onConfirm();
        this.hide();
        this.activeElement.focus();
      },
      deny: function deny() {
        this.onDeny();
        this.hide();
        this.activeElement.focus();
      },
    }
  });

  window.rightbar = new Vue({
    el: '#rightbar',
    data: {
      mode: 'saved',
    },
    methods: {
      setMode: function setMode(mode) {
        this.mode = mode;
      }
    }
  });

  window.topbar = new Vue({
    el: '#topbar',
    data: {
      active: false,
      displayTime: 2000,
      indicators: {
        bold: { active: false },
        italic: { active: false },
      }
    },
    created: function created() {
      this.debouncedHide = debounce(() => this.active = false, this.displayTime);
    },
    methods: {
      show: function show() {
        this.active = true;
        this.debouncedHide();
      },
      toggle: function toggle(zone) {
        this.indicators[zone].active = !this.indicators[zone].active;
        this.show();
      },
      activate: function activate(zone) {
        this.indicators[zone].active = true;
        this.show();
      },
      deactivate: function deactivate(zone) {
        this.indicators[zone].active = false;
        this.show();
      },
      deactivateAll: function deactivateAll() {
        for (let i in this.indicators) {
          this.deactivate(i);
        }
      }
    }
  });

  window.accountmenu = {
    template: document.querySelector('#accountmenutemplate').innerHTML,
    data: function() {
      return {
        mode: 'login',
        loginView: { pending: false, errors: [] },
        signupView: { pending: false, errors: [] },
        inputs: {
          username: '',
          email: '',
          password: '',
          confirmation: '',
        },
        projects: [],
        active: -1,
        editing: -1,
        search: '',
        user: null,
      };
    },
    directives: {
      focus: {
        inserted: function(el) {
          el.focus();
        }
      }
    },
    created: function created() {
      fetch('whoami').then((response) => {
        return response.json();
      })
      .then((data) => {
        if (Object.keys(data).length > 0) {
          this.user = data;
          this.mode = 'authenticated';
        }
      })
        .then(() => {

          if (this.mode !== 'authenticated') return;

          fetch('projects').then((response) => {
            return response.json();
          })
            .then((data) => {
              data.forEach((project) => {
                this.projects.push(project);
              });
              this.setActive(0);
            });
        });
    },
    computed: {
      filteredProjects: function filteredProjects() {
        return this.projects.filter((project) => {
          return project.name.includes(this.search);
        });
      },
    },
    methods: {
      prependNew: function prependNew() {
        fetch('new_project', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ name: 'new project' }),
        }).then((response) => {
          return response.json()
        })
          .then((data) => {
            this.projects.unshift(data);
            this.setActive(0);
            this.edit(0);
          });
      },
      setActive: function setActive(index) {
        let newIndex = Math.max(0, Math.min(this.projects.length - 1, index));
        let newProject = this.projects[newIndex];
        if (newProject == null) {
          this.active = -1;
          return;
        }

        fetch('bits', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ project_id: newProject.id }),
        }).then((response) => {
          return response.json()
        }).then((data) => {
          //TODO: properly nest data so children don't have to communicate
          window.leftmenu.$children[0].setBits(data)
          this.active = newIndex;
        });
      },
      handleKeydown: function handleKeydown(evt) {
        switch(evt.key) {
          case 'ArrowUp':
            if (this.editing > -1) break;
            this.setActive(this.active - 1);
            break;
          case 'ArrowDown':
            if (this.editing > -1) break;
            this.setActive(this.active + 1);
            break;
          case 'Delete':
            if (this.editing > -1) break;
            this.removeProject(this.active);
            break;
          case 'Insert':
            if (this.editing > -1) break;
            this.prependNew();
            break;
          case 'Enter':
            if (this.editing > -1) {
              evt.currentTarget.focus();
            }
            else {
              this.edit(this.active);
            }
            break;
          default:
            break;
        }
      },
      edit: function edit(index) {
        this.editing = index;
      },
      saveEdit: function saveEdit(evt) {
        if (!/\S/.test(this.projects[this.editing].name)) this.projects[this.editing].name = 'untitled';

        fetch('rename_project', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ project_id: this.projects[this.editing].id, name: this.projects[this.editing].name }),
        }).then((response) => {
          return response.json();
        }).then((data) => {
          this.editing = -1;
        });
      },
      removeProject: function removeProject(index) {
        window.modal.display({
          message: `Are you sure you want to delete "${this.projects[index].name}"? it will be gone forever.`,
          confirm: () => {
            this.deleteProject(index);
          },
        });
      },
      deleteProject: function removeProject(index) {
        fetch('delete_project', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ project_id: this.projects[index].id }),
        }).then((r) => (r.json())).then((data) => {
          this.projects.splice(index, 1);
          this.setActive((this.active < this.projects.length) ? this.active : this.projects.length - 1);
        });
      },
      gotoSignup: function gotoSignup(event) {
        event.preventDefault();
        this.mode = 'signup';
      },
      gotoLogin: function gotoLogin(event) {
        event.preventDefault();
        this.mode = 'login';
      },
      login: function login() {
        this.loginView.pending = true;
        this.loginView.errors = [];
        fetch('login', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ username: this.inputs.username, password: this.inputs.password }),
        }).then((response) => {
          return response.json();
        })
          .then((data) => {
            this.loginView.pending = false;
            if (data.errors) {
              this.loginView.errors = data.errors;
            }
            else {
              this.mode = 'authenticated';
              this.user = data;
              this.inputs = { username: '', email: '', password: '', confirmation: '' };
            }
          });
      },
      logout: function logout() {
        fetch('logout').then((result) => {
          this.mode = 'login';
          this.user = null;
        });
      },
      signup: function signup() {
        this.signupView.pending = true;
        this.signupView.errors = [];
        fetch('signup', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify(this.inputs),
        }).then((response) => {
          return response.json();
        }).then((data) => {
          this.signupView.pending = false;
          if (data.errors) {
            this.signupView.errors = data.errors;
          }
          else {
            this.mode = 'authenticated';
            this.user = data;
            this.inputs = { username: '', email: '', password: '', confirmation: '' };
          }
        });
      }
    }
  }

  window.projectmenu = {
    template: document.querySelector('#projectmenutemplate').innerHTML,
    data: function() { 
      return {
        active: -1,
        editing: -1,
        search: '',
        saved: true,
        autosaveInterval: null,
        bits : [] 
      };
    },
    directives: {
      focus: {
        inserted: function(el) {
          el.focus();
        }
      }
    },
    computed: {
      filteredBits: function filteredBits() {
        return this.bits.filter((bit) => {
          return bit.name.includes(this.search);
        });
      },
    },
    created: function created() {
      window.writeous.on('*', (data) => {
        if (data.type !== 'zonechange') {
          this.saved = false;
          window.rightbar.setMode('unsaved');
        }
      });

      this.autosaveInterval = setInterval(() => {
        if (!this.saved) this.saveActive();
      }, 30 * 1000);
    },
    methods: {
      setBits: function setBits(bits) {
        this.saveActive((data) => {
          this.bits = bits;
          this.setActive(0, false);
        });
      },
      onStart: function onStart(evt, originalEvent) {
        this.active = evt.oldIndex;
        this.refreshWriteous();
      },
      onEnd: function onEnd(evt, originalEvent) {
        this.active = evt.newIndex;
        this.refreshWriteous();
      },
      onAdd: function onAdd(evt, originalEvent) {
        evt.item.remove();
        this.insertNew(evt.newIndex);
      },
      endAdd: function(evt) {
        if (evt.srcElement === evt.to) evt.item.textContent = 'add';
      },
      insertNew: function insertNew(index) {
        //TODO: properly nest data so children don't have to communicate
        let accountmenu = window.leftmenu.$children[1];
        let project_id = accountmenu.projects[accountmenu.active].id;
        fetch('new_bit', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ project_id: project_id, name: 'untitled', index: index }),
        }).then((r) => r.json()).then((data) => {
          this.bits.splice(index, 0, { project_id: data.project_id, id: data.id, name: data.name, data: JSON.parse(data.data) });
          // if this is the first bit, save temp data
          if (this.bits.length === 1) {
            this.active = 0;
            this.bits[0].data = writeous.getData();
          }

          //FIXME: this probably causes another call
          this.setActive(index);
          this.edit(index);
        });
      },
      appendNew: function appendNew() {
        this.insertNew(this.bits.length);
        /*this.bits.push({ name: 'untitled', data: {} });

        // if this is the first bit, save temp data
        if (this.bits.length === 1) {
          this.active = 0;
          this.bits[0].data = writeous.getData();
        }

        this.setActive(this.bits.length - 1);*/

      },
      handleKeydown: function handleKeydown(evt) {
        switch(evt.key) {
          case 'ArrowUp':
            if (this.editing > -1) break;
            this.setActive(this.active - 1);
            break;
          case 'ArrowDown':
            if (this.editing > -1) break;
            this.setActive(this.active + 1);
            break;
          case 'Delete':
            if (this.editing > -1) break;
            this.removeBit(this.active);
            break;
          case 'Insert':
            if (this.editing > -1) break;
            this.insertNew(this.active + 1);
            break;
          case 'Enter':
            if (this.editing > -1) {
              evt.currentTarget.focus();
            }
            else {
              this.edit(this.active);
            }
            break;
          default:
            break;
        }
      },
      removeBit: function removeBit(index) {
        window.modal.display({
          message: `Are you sure you want to delete "${this.bits[index].name}"? it will be gone forever.`,
          confirm: () => {
            this.deleteBit(index);
          },
        });
      },
      deleteBit: function deleteBit(index) {
        fetch('delete_bit', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ bit_id: this.bits[index].id }),
        }).then((r) => (r.json())).then((data) => {
          this.bits.splice(index, 1);
          this.setActive((this.active < this.bits.length) ? this.active : this.bits.length - 1);
        });
      },
      setActive: function setActive(index, save = true) {
        let newIndex = Math.max(0, Math.min(this.bits.length - 1, index));
        let newBit = this.bits[index];

        let callback = () => {
          if (newBit == null) {
            this.active = -1;
            this.refreshWriteous();
            return;
          }

          fetch('bit', {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({ bit_id: newBit.id }),
          }).then((r) => r.json()).then((bitData) => {
            this.active = newIndex;
            this.bits[this.active].data = JSON.parse(bitData.data);
            this.refreshWriteous();
          });
        };

        if (save) {
          this.saveActive(callback);
        }
        else {
          callback();
        }
      },
      saveActive: function saveActive(callback) {
        let bit = this.bits[this.active];
        if (bit == null) return callback(null);

        let data = writeous.getData();
        this.bits[this.active].data = data;

        this.saved = true;
        window.rightbar.setMode('saving');

        fetch('save_bit', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ bit_id: bit.id, data: data }),
        }).then((r) => r.json()).then((data) => {
          let mode = (this.saved && data.errors == null) ? 'saved' : 'unsaved';
          window.rightbar.setMode(mode);
          if (callback) callback(data);
        });
      },
      refreshWriteous: function refreshWriteous() {
        let data = (this.bits[this.active] != null) ? this.bits[this.active].data : {};
        writeous.setData(data);
      },
      addBitClone: function addBitClone(event) {
        event.item.textContent = '~ add here ~';
        return event;
      },
      edit: function edit(index) {
        this.editing = index;
      },
      saveEdit: function saveEdit(evt) {
        if (!/\S/.test(this.bits[this.editing].name)) this.bits[this.editing].name = 'untitled';

        fetch('rename_bit', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: JSON.stringify({ bit_id: this.bits[this.editing].id, name: this.bits[this.editing].name }),
        }).then((r) => r.json()).then((data) => {
          this.editing = -1;
        });
      },

      log: function log() {
        console.log(...arguments);
      },
    }
  };

  window.leftmenu = new Vue({
    el: '#leftmenu',
    components: {
      'projectmenu': window.projectmenu,
      'accountmenu': window.accountmenu,
    },
    data: {
      hidden: false,
    },
    created: function created() {
      this.hide();
    },
    methods: {
      show: function show() {
        this.hidden = false;
        document.getElementById('site').style.gridTemplateColumns = '18em minmax(0, 1fr) 5em';
        writeous.autosizeBuffer();
      },
      hide: function hide() {
        this.hidden = true;
        document.getElementById('site').style.gridTemplateColumns = '5em minmax(0, 1fr) 5em';
        writeous.autosizeBuffer();
      },
    }
  });

  window.leftbar = new Vue({
    el: '#leftbar',
    data: {
      active: '',
      items: [
        'account',
        'project',
      ]
    },
    methods: {
      click: function click(item) {
        if (this.active === item) {
          window.leftmenu.hide();
          this.active = '';
        }
        else {
          window.leftmenu.show();
          this.active = item;
        }

        this.items.forEach((i) => document.querySelector(`#${i}menu`).style.display = 'none');
        document.querySelector(`#${item}menu`).style.display = '';
      }
    }
  });


  writeous.on('zonechange', (wevent) => {
    const zones = wevent.data;
    topbar.deactivateAll();
    zones.forEach((zone) => topbar.activate(zone));
  });

}

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

require 'pry'

require 'sequel'
require 'bcrypt'

class Database

  MINIMUM_PASSWORD_LENGTH = 20

  def initialize(connection_string)
    @db = Sequel.connect(connection_string)
  end

  def register_user(username, email, password, confirmation)
    errors = []

    unless password && password.length >= MINIMUM_PASSWORD_LENGTH
      errors << "password must be at least #{MINIMUM_PASSWORD_LENGTH} characters long"
    end

    if password != confirmation
      errors << 'passwords do not match'
    end

    if find_user(username: username)
      errors << 'username already exists'
    end

    if find_user(email: email)
      errors << 'email already being used'
    end

    return { errors: errors } unless errors.empty?

    BCrypt::Engine.cost = 13
    hash = BCrypt::Password.create(password)
    id = @db[:users].insert(username: username, email: email, hash: hash);
    return { username: username, email: email, id: id }
  end

  def find_user(parameters)
    return @db[:users].where(parameters).first
  end

  def authenticate(username, password)
    user = find_user(username: username)
    return { errors: ['bad credentials'] } unless user

    return { errors: ['bad credentials'] } unless BCrypt::Password.new(user[:hash]) == password

    return user.reject { |k, v| [:hash].include?(k) }
  end

  def projects(user)
    return { errors: ['must be logged in to access projects'] } unless user

    return @db[:projects].where(user_id: user['id']).order(Sequel.desc(:id)).to_a
  end

  def bits(user, project_id)
    project = @db[:projects].where(id: project_id).first
    return { errors: ['project not found'] } unless project

    unless project[:user_id] == user['id']
      return { errors: ['user not authorized to modify project'] }
    end

    return @db[:bits].select(:id, :index, :name, :project_id).where(project_id: project[:id]).to_a
  end

  def bit(user, bit_id) 
    bit = @db[:bits].where(id: bit_id).first
    return { errors: ['bit not found'] } unless bit

    project = @db[:projects].where(id: bit[:project_id]).first
    return { errors: ['user not authorized to access bit'] } unless project[:user_id] == user['id']

    return bit
  end

  def rename_bit(user, bit_id, name) 
    bit = @db[:bits].where(id: bit_id).first
    return { errors: ['bit not found'] } unless bit

    project = @db[:projects].where(id: bit[:project_id]).first
    return { errors: ['user not authorized to access bit'] } unless project[:user_id] == user['id']

    @db[:bits].where(id: bit_id).update(name: name)

    return { id: bit_id, name: name }
  end

  def save_bit(user, bit_id, data) 
    bit = @db[:bits].where(id: bit_id).first
    return { errors: ['bit not found'] } unless bit

    project = @db[:projects].where(id: bit[:project_id]).first
    return { errors: ['user not authorized to access bit'] } unless project[:user_id] == user['id']

    @db[:bits].where(id: bit_id).update(data: data.to_json)

    return { id: bit_id, data: data }
  end

  def new_project(user, name)
    return { errors: ['must be logged in to create a project'] } unless user

    project_id = -1
    bit_id = -1
    @db.transaction do
      project_id = @db[:projects].insert(name: name, user_id: user['id'])
      bit_id = @db[:bits].insert(name: 'untitled', project_id: project_id, data: '{}', index: 0)
    end

    return { id: project_id, name: name, bits: [{ id: bit_id, name: 'untitled', data: '{}', index: 0 }] }
  end

  def rename_project(user, project_id, name)
    return { errors: ['user not authorized to modify project'] } unless user

    @db[:projects].where(user_id: user['id'], id: project_id).update(name: name)

    return { id: project_id, name: name }
  end

  def delete_project(user, project_id)
    return { errors: ['user not authorized to modify project'] } unless user

    project = @db[:projects].where(id: project_id, user_id: user['id']).first

    return { errors: ['user not authorized to modify project'] } unless project

    @db.transaction do
      @db[:bits].where(project_id: project[:id]).delete
      @db[:projects].where(id: project[:id], user_id: user['id']).delete
    end

    return { id: project_id, deleted: true }
  end


  def new_bit(user, project_id, name, index = 0)
    project = @db[:projects].where(id: project_id).first
    return { errors: ['project not found'] } unless project

    unless project[:user_id] == user['id']
      return { errors: ['user not authorized to modify project'] }
    end

    bit_id = @db[:bits].insert(name: name, project_id: project_id, data: '{}', index: index)

    return { id: bit_id, name: name, project_id: project_id, data: '{}', index: index }
  end

  def delete_bit(user, bit_id)
    bit = @db[:bits].where(id: bit_id).first
    return { errors: ['bit not found'] } unless bit

    project = @db[:projects].where(id: bit[:project_id]).first
    return { errors: ['user not authorized to access bit'] } unless project[:user_id] == user['id']

    @db[:bits].where(id: bit_id).delete

    return { id: bit_id, deleted: true }
  end

end

DB = Database.new('sqlite://db/dev.db')


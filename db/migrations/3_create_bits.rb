Sequel.migration do
  change do
    create_table(:bits) do
      primary_key :id
      foreign_key :project_id, :projects
      Integer :index, null: false
      String :name, null: false
      String :data, text: true, default: '{}'
    end
  end
end

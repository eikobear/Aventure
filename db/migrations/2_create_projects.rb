Sequel.migration do
  change do
    create_table(:projects) do
      primary_key :id
      foreign_key :user_id, :users
      String :name, null: false
    end
  end
end
